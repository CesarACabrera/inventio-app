 
#Inventio 1.0.0

##Características:

* Organizar notas por categorías: cada categoría debe poder crear grupos y estos a su vez crear diversas notas en lista
* Debe poder organizar por notas recientes y favoritas
* Las notas deben tener la fecha de cuándo se escribieron
* Las notas se deben poder mover de categoría
* De ser posible poner un buscador
* Opción de agregar un tema oscuro
* Añadir imágenes y fotos
* De ser posible poder formatear texto (si es poesía que sea centrado o narrativa justificado, cursivas, etc.)
* Exportar notas a TXT por carpeta, documento o de forma individual (o a ODT si es posible)

La configuración debe ir en un JSON.  
Cada nota debe ser un objeto.
