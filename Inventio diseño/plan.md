#Plan

##General:

Crear la UX primero (todas las A’s, dividido en pasos).  
Crear el filesystem:  

* Crear, borrar y renombrar carpeta.
* Crear, borrar, mover y renombrar documento.
* Crear, borrar, mover y renombrar nota.

Integrar el paso anterior al UX.  

Programar la cámara (y de ser posible la galería).  
Programar la lógica de mostrar (solo mostrar) “Recientes” y “Favoritos”.  
Crear el sistema de exportaciones:  
 
* Exportar carpeta.
* Exportar documento.
* Exportar nota.
* Exportar todo.

Integrar el paso anterior a la UX (incluye integrar el paso anterior a exportar “Recientes” y “Favoritos”).  
Programar el motor de búsqueda  
Agregar el paso anterior a la UX  
Programar el tema oscuro.  

##Desglose:

* A1 (XML).
* A4 (XML).
* A5 (XML).
* Crear, borrar y renombrar carpeta.
* Crear, borrar, mover y renombrar documento.
* Crear, borrar, mover y renombrar nota.
* B1 (XML y JS).
* B2 (XML y JS).
* B3 (XML y JS).
* D1 – 3 (XML).
* F2 (XML y JS).
* F3 (XML y JS).
* G3 (XML y JS).
* H1 (XML y JS).
* H2 (XML y JS).
* H3 (XML y JS).
* Programar la cámara (y de ser posible la galería).
* Guardar imagen en la nota (filesystem).
* A4 (JS).
* A5 (JS).
* Exportar simple:
    - Exportar nota.
    - Exportar documento.
* Exportar carpeta.
* Exportar todo.
* E1 (XML y JS).
* E2 (XML y JS).
* E3 (XML y JS).
* D4 (=D5) (XML y JS).
* D7 (XML y JS).
* Programar el botón de búsqueda en el filesystem.
* A6 (XML y JS).
* B7.
