import { Application, Button, Dialogs, EventData, Folder, ItemEventData, knownFolders, Label, Page, path, Screen, ShowModalOptions, ViewBase } from "@nativescript/core";
import { RadListView } from "nativescript-ui-listview";
import { exportFile, exportFolder, exportNote } from "./export";
import { createFile, createFolder, deleteNote, getFavoriteNotes, getFiles, getFolders, getMatchNotes, getNotes, getRecentNotes, moveFile, Note, NoteFileStructure, removeFile, removeFolder, renameFile, renameFolder, replaceNote, saveNotes } from "./fileSystem";
import * as permissions from 'nativescript-permissions';

type CreateContext = { fileType: string, hintType: string };

export interface State {
    context: { text: string, fileType: string }[],
    page: Page,
    createContext?: CreateContext,
    extras?: string[]
}

export function reorderIndex(notes: Note[]): void {
    notes.forEach((note, index) => {
        note.index = index;
    });
}

export function fab(page: Page): void {
    const fab: ViewBase = page.getViewById("fab");
    fab.top = Screen.mainScreen.heightDIPs - 270;
    fab.left = Screen.mainScreen.widthDIPs - 80;
};

export function createEntity(page: Page, args: EventData, createContext: CreateContext, folderName?: string): void {
    const fileType = createContext.fileType;

    const mainView: Button = <Button>args.object;

    const options: ShowModalOptions = {
        context: createContext,
        closeCallback: async (name: string) => {
            if (fileType === "a carpeta") {
                createFolder(name);
                page.bindingContext.set("folders", await getFolders());
            }

            if (fileType === "o documento") {
                createFile(folderName, `${name}.json`);
                page.bindingContext.set("documents", await getFiles(folderName));
            }

        },
        cancelable: true
    }

    mainView.showModal("~/modals/createTemplate/createTemplate", options);
}

export function fsOptionsModal(args: EventData, state: State): void {
    const radList = <RadListView>args.object;

    const options: ShowModalOptions = {
        context: state.context,
        closeCallback: (option: string) => {
            fsOptionsCallback(option, radList.getSelectedItems()[0], radList, state);
        },
        cancelable: true
    }

    radList.showModal("~/modals/fsOptions/fsOptionsFrame", options);
};

async function fsOptionsCallback(option: string, element: any, radList: RadListView, state: State) {
    const page = state.page;
    const fileType = state.context[0].fileType;
    const extras = state.extras;

    if (option === "Exportar ") {
        permissions.requestPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE).then(
            async function success() {
                if (fileType === "carpeta") {
                    exportedDialog(fileType, await exportFolder(element));
                }

                if (fileType === "documento") {
                    exportedDialog(fileType, await exportFile(element));
                }

                if (fileType === "nota") {
                    exportedDialog(fileType, await exportNote(element));
                }

            },
            function error() {
                //console.log("Permission denied.");
            }
        );
    }

    if (option === "Renombrar ") {
        state.createContext.fileType = "o nombre";

        const options: ShowModalOptions = {
            context: state.createContext,
            closeCallback: async (newName: string) => {
                if (fileType === "carpeta") {
                    renameFolder(element.name, newName);
                    page.bindingContext.set("folders", await getFolders());
                }

                if (fileType === "documento") {
                    renameFile(element.path, newName);
                    page.bindingContext.set("documents", await getFiles(extras[0]));
                }
            },
            cancelable: true
        }

        page.showModal("~/modals/createTemplate/createTemplate", options);
    }

    if (option === "Mover ") {
        const folders: string[] = await (await getFolders()).map(entity => entity.name);

        const moveOptions: ShowModalOptions = {
            context: folders,
            closeCallback: async (folder: string) => {
                if (fileType === "documento") {
                    moveFile(element.path, element.name, folder);
                    page.bindingContext.set("documents", await getFiles(extras[0]));
                }

                if (fileType === "nota") {
                    secondMove(page, folder, element.index, extras[0]);
                }
            },
            cancelable: true
        }

        page.showModal("~/modals/fileSelector/fileSelectorFrame", moveOptions);
    }

    if (option === "Borrar ") {
        const article: string = fileType === "carpeta" ? "la" : "el";
        const defaultMessage: string = `¿Seguro que quieres borrar ${article} ${fileType} ${element.name}?`
        const noteMessage: string = "¿Seguro que quieres borrar esta nota?";
        const confirmOptions = {
            title: `Borrar ${fileType}`,
            message: `${fileType === "nota" ? noteMessage : defaultMessage}`,
            okButtonText: "Aceptar",
            neutralButtonText: "Cancelar"
        }

        Dialogs.confirm(confirmOptions).then(async result => {
            if (result) {
                if (fileType === "carpeta") {
                    removeFolder(element.name);
                    page.bindingContext.set("folders", await getFolders());
                }

                if (fileType === "documento") {
                    removeFile(element.path);
                    page.bindingContext.set("documents", await getFiles(extras[0]));
                }

                if (fileType === "nota") {
                    deleteNote(element.id, element.path);
                    page.bindingContext.set("notes", await setContext(extras[0], element.path));
                }
            }
        });
    }

    radList.deselectAll();
}

async function secondMove(page: Page, folder: string, index: number, pageType: string) {
    const mainFolder: Folder = <Folder>knownFolders.documents().getFolder("folders");
    const noteToMove: Note = page.bindingContext.notes[index];
    const oldPath: string = noteToMove.path;

    if (folder) {
        const documents: string[] = await (await getFiles(folder)).map(entity => entity.name);

        const options: ShowModalOptions = {
            context: documents,
            closeCallback: async (document: string) => {
                const filePath: string = path.join(mainFolder.path, folder, document);

                const newNotes: NoteFileStructure = await getNotes(filePath);
                noteToMove.path = filePath;
                newNotes.notes.push(noteToMove);
                reorderIndex(newNotes.notes);
                saveNotes(filePath, newNotes);

                deleteNote(noteToMove.id, oldPath);
                page.bindingContext.set("notes", await setContext(pageType, oldPath));
            },
            cancelable: true
        }

        if (documents.length === 0) {
            const alertOptions = {
                title: "Error al mover la nota",
                message: "Esta carpeta está vacia. Por favor elija una carpeta que tenga por lo menos 1 documento.",
                okButtonText: "Aceptar",
                cancelable: false
            }

            Dialogs.alert(alertOptions);
        } else {
            page.showModal("~/modals/fileSelector/fileSelectorFrame", options);
        }
    }
}

export function setFavorite(page: Page, args: EventData) {
    const starLabel = <Label>args.object;
    const index: number = parseInt(starLabel.id);

    const note: Note = page.bindingContext.notes[index];
    note.favorites = !note.favorites;

    replaceNote(note, note.id);

    const radListView = <RadListView>starLabel.parentNode.parentNode.parentNode.parentNode;
    radListView.refresh();
}

export function editorModal(page: Page, args: ItemEventData, documentPath?: string) {
    const mainView: Button = <Button>args.object;

    let index: number;
    let edit: boolean;
    let path: string = documentPath;

    if (args.index >= 0) {
        index = args.index;
        edit = false;
        path = page.bindingContext.notes[index].path;
    } else {
        const date = new Date();
        page.bindingContext.notes.push(new Note(page.bindingContext.notes.length, "Nueva nota", date.toDateString(), null, false, path));
        index = page.bindingContext.notes.length - 1;
        edit = true;
    }

    const options: ShowModalOptions = {
        context: {
            notes: page.bindingContext.notes,
            index,
            path: path,
            edit
        },
        closeCallback: async () => {
            let setNotes = await getRecentNotes();

            if (documentPath) {
                setNotes = await getNotes(path).notes;
            }

            page.bindingContext.set("notes", setNotes);
        },
        fullscreen: true,
        animated: true
    }

    mainView.showModal("~/modals/noteEditor/editorFrame", options);
}

export async function setContext(pageType: string, path: string) {
    if (pageType === "notes") {
        return await getNotes(path).notes;
    } else if (pageType === "recent") {
        return await getRecentNotes();
    } else if (pageType === "favorites") {
        return await getFavoriteNotes();
    } else {
        return await getMatchNotes(pageType);
    }
}

export function exportedDialog(type: string, path: string) {
    const alertOptions = {
        title: `${type.charAt(0).toUpperCase() + type.slice(1)} exportad${type.slice(-1)} en:`,
        message: path,
        okButtonText: 'Aceptar',
        cancelable: true
    }

    Dialogs.alert(alertOptions);
}