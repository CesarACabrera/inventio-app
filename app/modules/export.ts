import { File, FileSystemEntity, Folder, ImageSource, path } from "@nativescript/core";
import { getFiles, getFolders, getNotes, Note } from "./fileSystem";
import { Document, IImageOptions, ImageRun, Packer, Paragraph, TextRun } from "docx";
import { IPropertiesOptions } from "docx/build/file/core-properties";

const downloadPath = path.join(android.os.Environment.getExternalStoragePublicDirectory(android.os.Environment.DIRECTORY_DOWNLOADS).toString(), "Notas");


function convertBlob(data: Blob) {
    const blob = (Array as any).create("byte", data.size);

    for (let i = 0; i < blob.length; i++) {
        blob[i] = (data as any)._buffer[i];
    }

    return blob;
}

function setDocStructure(note: Note, docOptions: IPropertiesOptions) {
    docOptions.sections[0].children.push(
        new Paragraph({
            children: [new TextRun({ text: note.date, italics: true })],
            spacing: {
                after: 200
            }
        })
    );

    docOptions.sections[0].children.push(
        new Paragraph({
            text: note.text,
            spacing: {
                after: 200
            }
        })
    );

    if (typeof note.image === "string") {
        const imageData = ImageSource.fromFileSync(note.image).toBase64String("jpg");

        const imageOptions: IImageOptions = {
            data: imageData,
            transformation: {
                width: 300,
                height: 300
            }
        }

        const image = new ImageRun(imageOptions);
        docOptions.sections[0].children.push(
            new Paragraph({
                children: [image]
            })
        );
    }
}

async function writeFile(docOptions: IPropertiesOptions, exportedFile: File): Promise<string> {
    const doc = new Document(docOptions);
    const data = await Packer.toBlob(doc);
    const binarySource = convertBlob(data);
    exportedFile.writeSync(binarySource, err => {/*console.log("err: ", err)*/ });
    return exportedFile.path;
}

export async function exportNote(note: Note): Promise<string> {
    const downloadFolder: Folder = Folder.fromPath(downloadPath);
    const filePath: string = path.join(downloadFolder.path, `${note.text.substring(0, 20)}.docx`);
    const exportedFile: File = File.fromPath(filePath);

    let docOptions: IPropertiesOptions = {
        sections: [
            {
                children: []
            }
        ]
    }

    setDocStructure(note, docOptions);

    const doc = new Document(docOptions);

    return await writeFile(docOptions, exportedFile);
}

export async function exportFile(file: FileSystemEntity, folderPath = downloadPath): Promise<string> {
    const filePath: string = path.join(folderPath, `${file.name.substr(0, file.name.length - 5)}.docx`);
    const exportedFile: File = File.fromPath(filePath);

    const notes = getNotes(file.path).notes;

    let docOptions: IPropertiesOptions = {
        sections: [
            {
                children: []
            }
        ]
    }

    notes.forEach(note => {
        setDocStructure(note, docOptions)
    });

    return await writeFile(docOptions, exportedFile);
}

export async function exportFolder(element: FileSystemEntity): Promise<string> {
    const downloadFolder: Folder = Folder.fromPath(downloadPath);
    const exportedFolderPath = path.join(downloadFolder.path, element.name);
    const exportedFolder: Folder = Folder.fromPath(exportedFolderPath);

    const files = await getFiles(element.name);

    const promises = [];

    files.forEach(file => {
        promises.push(exportFile(file, exportedFolder.path));
    });

    return Promise.all(promises).then(() => {
        return exportedFolderPath;
    });
}

export async function exportAllNotes(notes: Note[], type: string): Promise<string> {
    const filePath: string = path.join(downloadPath, `${type}.docx`);
    const exportedFile: File = File.fromPath(filePath);

    let docOptions: IPropertiesOptions = {
        sections: [
            {
                children: []
            }
        ]
    }

    notes.forEach(note => {
        setDocStructure(note, docOptions)
    });

    return await writeFile(docOptions, exportedFile);
}

export async function exportAllFolders(): Promise<string> {
    const folders = await getFolders();
    const promises = [];

    folders.forEach(folder => {
        promises.push(exportFolder(folder));
    });

    return Promise.all(promises).then(() => {
        return downloadPath;
    });
}

