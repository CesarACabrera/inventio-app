import { knownFolders, Folder, File, FileSystemEntity } from "@nativescript/core/file-system";
import { reorderIndex } from "./utilities";
import md5 from "md5";

export interface NoteStructure {
    index: number,
    text: string,
    date: string,
    image: string | boolean,
    favorites: boolean
}

export interface NoteFileStructure {
    notes: Note[]
}



export class Note implements NoteStructure {
    readonly id: string;
    constructor(
        public index: number,
        public text: string,
        public date: string,
        public image: string | null,
        public favorites: boolean,
        public path: string,
    ) {
        this.id = md5(this.index, this.text, this.date, this.image, this.favorites, this.path);
    }
}

const folders: Folder = <Folder>knownFolders.documents().getFolder("folders");

export function createFolder(name: string): Folder {
    const newFolder: Folder = <Folder>folders.getFolder(name);
    return newFolder;
}

export function removeFolder(name: string): Promise<any> {
    const folderToRemove: Folder = <Folder>folders.getFolder(name);
    return folderToRemove.remove();
}

export function renameFolder(folderName: string, newName: string): Promise<any> {
    const folderToRename: Folder = <Folder>folders.getFolder(folderName);
    return folderToRename.rename(newName);
}

export function getFolders(): Promise<FileSystemEntity[]> {
    return folders.getEntities();
}

export function createFile(folderName: string, fileName: string): File {
    const newFile: File = <File>folders.getFolder(folderName).getFile(fileName);
    const notes: NoteFileStructure = {
        "notes": []
    };
    newFile.writeTextSync(JSON.stringify(notes));
    return newFile;
}

export function removeFile(path: string): Promise<any> {
    const fileToRemove: File = File.fromPath(path);
    return fileToRemove.remove();
}

export function renameFile(path: string, newName: string): Promise<any> {
    const fileToRename: File = File.fromPath(path);
    return fileToRename.rename(newName);
}

export function moveFile(path: string, fileName: string, newFolder: string): File {
    const fileToMove: File = File.fromPath(path);
    let fileContent = fileToMove.readTextSync();
    const fileJSON: NoteFileStructure = JSON.parse(fileContent);
    const newFile = createFile(newFolder, fileName);
    fileJSON.notes.forEach(note => {
        note.path = newFile.path;
    });
    fileContent = JSON.stringify(fileJSON);
    newFile.writeTextSync(fileContent);

    if (path !== newFile.path) {
        removeFile(path);
    }

    return newFile;
}

export function getFiles(folderName: string): Promise<FileSystemEntity[]> {
    return folders.getFolder(folderName).getEntities();
}

export function getNotes(path: string): NoteFileStructure {
    const file: File = File.fromPath(path);
    const fileContent = file.readTextSync();
    return JSON.parse(fileContent);
}

export function saveNotes(path: string, notes: NoteFileStructure): void {
    const file: File = File.fromPath(path);
    file.writeTextSync(JSON.stringify(notes));
}

export function copyNote(path: string, note: Note): void {
    const file: File = File.fromPath(path);
    const notesJSON: NoteFileStructure = JSON.parse(file.readTextSync());
    notesJSON.notes.push(note);
    saveNotes(file.path, notesJSON);
}

export function deleteNote(id: string, oldPath: string): void {
    const notes: Note[] = getNotes(oldPath).notes;

    const index = notes.indexOf(notes.find(note => note.id === id));

    notes.splice(index, 1);
    reorderIndex(notes);

    saveNotes(oldPath, { notes: notes });
}

export async function replaceNote(newNote: Note, id: string): Promise<void> {
    const notes = getNotes(newNote.path).notes;
    const noteToReplace = notes.find(note => note.id === id);
    Object.keys(newNote).forEach(key => noteToReplace[key] = newNote[key]);
    reorderIndex(notes);
    saveNotes(newNote.path, { notes: notes });
}

export function getAllNotes(): Promise<Note[]> {
    return new Promise(resolve => {
        const fullNotes: Note[] = [];

        getFolders().then(folders => {
            folders.forEach(folder => {
                getFiles(folder.name).then(files => {
                    files.forEach(file => {
                        const noteStructure: NoteFileStructure = getNotes(file.path);
                        noteStructure.notes.forEach(note => {
                            fullNotes.push(note);
                        });
                    });

                    resolve(fullNotes);
                });
            });
        });
    });
}

export async function getRecentNotes(): Promise<Note[]> {
    const recentNotes: Note[] = (await getAllNotes()).sort((firstNote, secondNote) => {
        return Date.parse(secondNote.date) - Date.parse(firstNote.date);
    });

    reorderIndex(recentNotes);

    return recentNotes;
}

export async function getFavoriteNotes(): Promise<Note[]> {
    const favoriteNotes: Note[] = (await getAllNotes()).filter(note => note.favorites === true);

    reorderIndex(favoriteNotes);

    return favoriteNotes;
}

export async function getMatchNotes(text: string): Promise<Note[]> {
    const notes: Note[] = await getAllNotes();

    const result: Note[] = notes.filter(note => note.text.match(text));

    return result;
}