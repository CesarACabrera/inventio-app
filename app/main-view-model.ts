import { File, Folder, knownFolders, Observable, path } from "@nativescript/core";

const folders: Folder = <Folder>knownFolders.documents();
const filePath: string = path.join(folders.path, "theme");
let themeFile: File;

if (!File.exists(filePath)) {
    themeFile = File.fromPath(filePath);
    themeFile.writeTextSync("ns-light");
}

themeFile = File.fromPath(filePath);
const savedTheme: string = themeFile.readTextSync();


class SetTheme {
    private theme: string;

    constructor() {
        this.theme = savedTheme;
    }

    get getTheme() {
        return this.theme;
    }

    changeTheme() {
        this.theme = this.theme === "ns-light" ? "ns-dark" : "ns-light";
        themeFile.writeTextSync(this.theme);
    }
}

export const setTheme = new SetTheme();