import { fromObject, Page, ShownModallyData } from "@nativescript/core";
import { setTheme } from "~/main-view-model";
let closeCallback: Function;
let page: Page;

export function onShownModally(args: ShownModallyData) {
    const context = args.context;
    page = <Page>args.object;
    page.bindingContext = fromObject({
        ...context,
        fileName: "",
        theme: setTheme.getTheme
    });

    closeCallback = args.closeCallback;
}

export function onTapClose() {
    closeCallback(page.bindingContext.get("fileName"));
}