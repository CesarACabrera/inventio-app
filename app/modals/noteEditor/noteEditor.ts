import { Dialogs, fromObject, Image, Label, Page, ShownModallyData } from "@nativescript/core";
import { Note, replaceNote, saveNotes } from "~/modules/fileSystem";
import * as camera from '@nativescript/camera';
import * as imagePicker from "@nativescript/imagepicker";

let page: Page;
let notes: Note[];
let id: string;
let newNote;

export function onLoaded(args: ShownModallyData) {
    page = <Page>args.object;

    notes = page.frame.bindingContext.notes;
    const index = page.frame.bindingContext.index;

    page.bindingContext = fromObject({
        visibility: "collapsed",
        note: notes[index],
        path: page.frame.bindingContext.path,
        doneText: "Editar",
        edit: page.frame.bindingContext.edit
    });

    if (page.bindingContext.edit) {
        page.bindingContext.visibility = "visible";
        page.bindingContext.doneText = "Listo";
    }

    newNote = page.frame.bindingContext.edit;
    id = page.bindingContext.note.id;
}

function generateNotes() {
    const note = page.bindingContext.note;

    if (newNote) {
        saveNotes(note.path, { notes: notes });
    } else {
        const date = new Date();
        note.date = date.toDateString();
        replaceNote(note, id);
    }

}

export function onDoneTap() {
    const note: Note = page.bindingContext.note;
    const visibility = page.bindingContext.visibility;
    const doneText = page.bindingContext.doneText;
    page.bindingContext.set("visibility", visibility === "visible" ? "collapsed" : "visible");
    page.bindingContext.set("doneText", doneText === "Listo" ? "Editar" : "Listo");

    const textLabel = <Label>page.getViewById("textLabel");
    textLabel.text = note.text;

    if (page.bindingContext.edit) {
        generateNotes();
        id = note.id;
    }

    page.bindingContext.edit = !page.bindingContext.edit;

}

export function onPictureTap() {
    const actionOptions = {
        title: "Añadir imagen",
        message: "Selecciona una fuente:",
        cancelButtonText: "Cancelar",
        actions: ["Cámara", "Galería"],
        cancelable: true
    }

    Dialogs.action(actionOptions).then(result => {
        if (result === 'Cámara') {
            onCameraTap();
        }

        if (result === 'Galería') {
            onPickerTap();
        }
    });
}

function onCameraTap() {
    const image = <Image>page.getViewById("noteImage");
    const options = {
        width: 300,
        height: 300,
        keepAspectRatio: true,
        saveToGallery: true
    }

    const isCameraAvailable: Boolean = camera.isAvailable();

    if (isCameraAvailable) {
        camera.requestPermissions().then(
            function success() {
                camera
                    .takePicture(options)
                    .then(imageAsset => {
                        page.bindingContext.note.image = imageAsset.android;
                        image.src = imageAsset.android;
                        generateNotes();
                    })
                    .catch(error => {
                        //console.log("Error: ", error);
                    });
            },
            function error() {
                //console.log("camera error");
            }
        );
    } else {
        const alertOptions = {
            title: "Cámara no disponible",
            message: "Este dispositivo no cuenta con una cámara disponible para tomar fotos.",
            okButtonText: "Aceptar",
            cancelable: true
        }

        Dialogs.alert(alertOptions);
    }
}

function onPickerTap() {
    const image = <Image>page.getViewById("noteImage");
    const context = imagePicker.create({
        mode: "single"
    });

    context.authorize()
        .then(() => {
            return context.present();
        })
        .then(selection => {
            page.bindingContext.note.image = selection[0].android;
            image.src = selection[0].android;
            generateNotes();

        })
        .catch(error => {
            throw error;
        });
}

export function onLongPress() {
    const image = <Image>page.getViewById("noteImage");

    const confirmOptions = {
        title: "Borrar imagen",
        message: "¿Seguro que deseas borrar esta imagen?",
        okButtonText: "Sí",
        neutralButtonText: "Cancelar"
    }

    Dialogs.confirm(confirmOptions).then(result => {
        if (result) {
            page.bindingContext.note.image = null;
            generateNotes();
            image.src = null;
        }
    });
}