import { fromObject, ItemEventData, Page, ShownModallyData } from "@nativescript/core";

let closeCallback: Function;
let page: Page;
let context: string[];

export function onLoaded(args: ShownModallyData) {
    page = <Page>args.object;
    context = page.frame.bindingContext.context;
    page.bindingContext = fromObject({
        options: context
    });

    closeCallback = page.frame.bindingContext.closeCallback;
}

export function onItemTap(args: ItemEventData) {
    closeCallback(page.bindingContext.options[args.index].text);
}