import { EventData, fromObject, ItemEventData, Page, SearchBar } from "@nativescript/core";
import { getMatchNotes, Note } from "~/modules/fileSystem";
import { editorModal, fsOptionsModal, setFavorite, State } from "~/modules/utilities";

let page: Page;
let text: string;

export async function onLoaded(args: EventData) {
    page = <Page>args.object;
    page.bindingContext = fromObject({
        notes: []
    });
}

export async function onSubmit(args: EventData) {
    const searchBar: SearchBar = <SearchBar>args.object;
    text = searchBar.text;

    const matchedNotes: Note[] = await getMatchNotes(text);

    page.bindingContext.set("notes", matchedNotes);
}

export function onStarTap(args: EventData) {
    setFavorite(page, args);
}

export function openEditor(args: ItemEventData) {
    editorModal(page, args);
}

export function onItemSelected(args: EventData) {
    const state: State = {
        context: [
            { text: "Exportar ", fileType: "nota" },
            { text: "Mover ", fileType: "nota" },
            { text: "Borrar ", fileType: "nota" }
        ],
        page: page,
        extras: [text]
    }
    fsOptionsModal(args, state);
}

export function closeModal() {
    page.frame.bindingContext.closeCallback()
}
