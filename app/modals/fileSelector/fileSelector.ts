import { EventData, fromObject, Page, ShownModallyData } from "@nativescript/core";
let closeCallback: Function;
let page: Page;

export function onLoaded(args: ShownModallyData) {
    page = <Page>args.object;
    page.bindingContext = fromObject({
        entities: page.frame.bindingContext.context,
        index: 0
    });

    closeCallback = page.frame.bindingContext.closeCallback;
}

export function onDoneTap(args: EventData) {
    const index: number = page.bindingContext.index;
    closeCallback(page.bindingContext.entities[index]);
}