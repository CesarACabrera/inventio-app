import { fromObject, Page, ShownModallyData } from "@nativescript/core";
import { setTheme } from "~/main-view-model";

export function onShownModally(args: ShownModallyData) {
    const page = <Page>args.object;
    page.bindingContext = fromObject({
        context: args.context,
        closeCallback: args.closeCallback,
        theme: setTheme.getTheme
    });
}