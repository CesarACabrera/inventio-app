import { File, FileSystemEntity, Folder, knownFolders, path } from "@nativescript/core";
import { exportAllFolders, exportAllNotes, exportFile, exportFolder, exportNote } from "~/modules/export";
import { Note, NoteFileStructure, saveNotes } from "~/modules/fileSystem";

const folders: Folder = <Folder>knownFolders.documents().getFolder("folders");
const testFolder = Folder.fromPath(path.join(folders.path, "Test"));

const downloadPath = path.join(android.os.Environment.getExternalStoragePublicDirectory(android.os.Environment.DIRECTORY_DOWNLOADS).toString(), "Notas");
const downloadFolder: Folder = Folder.fromPath(downloadPath);



const noteStructure: NoteFileStructure = {
    notes: [
        new Note(0, "test", new Date().toString(), null, false, "test path"),
        new Note(0, "test 2", new Date().toString(), null, false, "test path 2")
    ]
}

describe("Export", () => {
    it("exportNote should export a note in /Downloads folder in a .docx format and return the file's path.", async () => {
        const exportedFile: File = File.fromPath(path.join(downloadPath, "test.docx"));
        const exportedPath: string = await exportNote(noteStructure.notes[0]);

        const result: boolean = exportedPath === exportedFile.path;
        const extension = exportedPath.substr(exportedPath.length - 5, exportedPath.length);

        assert.equal(result === true && extension === ".docx" && exportedFile.size > 0, true);
    });

    it("exportFile should export a file in /Downloads folder in a .docx format and return the file's path.", async () => {
        const testFile: File = File.fromPath(path.join(testFolder.path, "testFile.json"));

        saveNotes(testFile.path, noteStructure);

        const exportedFile: File = File.fromPath(path.join(downloadPath, "testFile.docx"));
        const exportedPath: string = await exportFile(testFile);

        const result: boolean = exportedPath === exportedFile.path;
        const extension = exportedPath.substr(exportedPath.length - 5, exportedPath.length);

        assert.equal(result === true && extension === ".docx" && exportedFile.size > 0, true);
    });

    it("exportFolder should export a folder in /Downloads folder and return the folder's path.", async () => {
        const exportedFolder: Folder = Folder.fromPath(path.join(downloadPath, "Test"));
        const exportedPath: string = await exportFolder(testFolder);

        const result: boolean = exportedPath === exportedFolder.path;
        const entities: FileSystemEntity[] = exportedFolder.getEntitiesSync();

        assert.equal(result === true && entities.length > 0, true);
    });

    it("exportAllNotes should export all notes in /Downloads folder in a file with .docx format and return the file's path.", async () => {
        const testFile: File = File.fromPath(path.join(testFolder.path, "recent.json"));

        saveNotes(testFile.path, noteStructure);

        const exportedFile: File = File.fromPath(path.join(downloadPath, "recent.docx"));
        const exportedPath: string = await exportAllNotes(noteStructure.notes, "recent");

        const result: boolean = exportedPath === exportedFile.path;
        const extension = exportedPath.substr(exportedPath.length - 5, exportedPath.length);

        assert.equal(result === true && extension === ".docx" && exportedFile.size > 0, true);
    });

    it("exportAllFolders should export all folder in /Downloads folder and return downloadPath.", async () => {
        const exportedFolder: Folder = Folder.fromPath(path.join(downloadPath, "Test"));
        const exportedPath: string = await exportAllFolders();

        const result: boolean = exportedPath === downloadPath;

        assert.equal(result === true, true);
    });
});