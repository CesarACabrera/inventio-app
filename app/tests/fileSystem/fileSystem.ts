import { copyNote, createFile, createFolder, deleteNote, getAllNotes, getFavoriteNotes, getFiles, getFolders, getMatchNotes, getNotes, getRecentNotes, moveFile, Note, NoteFileStructure, removeFile, removeFolder, renameFile, renameFolder, replaceNote, saveNotes } from "~/modules/fileSystem";
import { knownFolders, Folder, File, path, FileSystemEntity } from "@nativescript/core/file-system";

const folders: Folder = <Folder>knownFolders.documents().getFolder("folders");

describe("Folders", () => {
    it("createFolder should create a new folder in documents/folders.", () => {
        const testFolder: Folder = createFolder("testFolder");
        const result: boolean = Folder.exists(testFolder.path);
        assert.equal(result, true);
    });

    it("removeFolder should remove a existing folder.", () => {
        const renamedFolderPath: string = folders.getFolder("removeFolder").path;
        removeFolder("removeFolder").then(() => {
            const result: boolean = Folder.exists(renamedFolderPath);
            assert.equal(result, false);
        });
    });

    it("renameFolder should rename an existing folder.", () => {
        renameFolder("testFolder", "renamedFolder").then(() => {
            const renamedFolderPath: string = folders.getFolder("renamedFolder").path;
            const result: boolean = Folder.exists(renamedFolderPath);
            assert.equal(result, true);
        });
    });

    it("getFolders should get an array of existing folders.", async () => {
        const folders: FileSystemEntity[] = await getFolders();
        const result: boolean = Array.isArray(folders);
        assert.equal(result, true);
    });
});

describe("Files", () => {
    it("createFile should create a new JSON file in the specified folder.", () => {
        const fileTests: Folder = createFolder("fileTests");
        const testJSON: File = createFile("fileTests", "test.json");
        const resultOne: boolean = File.exists(testJSON.path);

        const fileContent: NoteFileStructure = getNotes(testJSON.path);
        const resultTwo: boolean = fileContent.hasOwnProperty('notes');

        assert.equal(resultOne && resultTwo, true);
    });

    it("removeFile should remove an existing file in a specific folder.", () => {
        const toRemoveFilePath: string = folders.getFolder("fileTests").getFile("remove.json").path;
        removeFile(toRemoveFilePath).then(() => {
            const result: boolean = File.exists(toRemoveFilePath);
            assert.equal(result, false);
        });
    });

    it("renameFile should rename an existing file in a specific folder.", () => {
        const filePath: string = path.join(folders.path, "fileTests", "test.json")
        renameFile(filePath, "renamed.json").then(() => {
            const renamedFilePath: string = folders.getFolder("fileTests").getFile("renamed.json").path;
            const result: boolean = File.exists(renamedFilePath);
            assert.equal(result, true);
        });
    });

    it("moveFile should move an existing file from one folder to another.", () => {
        const moveTestsFolder: Folder = createFolder("moveTests");
        const originalPath: string = createFile("fileTests", "moveFile.json").path;
        const newPath = moveFile(originalPath, "moveFile.json", "moveTests").path;

        const resultOne: boolean = File.exists(originalPath);
        const resultTwo: boolean = File.exists(newPath);

        assert.equal(resultOne === false && resultTwo === true, true);
    });

    it("getFiles should get an array of existing files.", async () => {
        const files: FileSystemEntity[] = await getFiles("fileTests");
        const result: boolean = Array.isArray(files);

        assert.equal(result, true);
    });
});

describe("Notes", () => {
    it("getNotes should get an object with interface NoteFileStructure", () => {
        const fileToGet: File = <File>folders.getFolder("fileTests").getFile("notes.json");
        const notesJSON: NoteFileStructure = { notes: [] };
        fileToGet.writeTextSync(JSON.stringify(notesJSON));
        const notes: NoteFileStructure = getNotes(fileToGet.path);
        const result: boolean = Array.isArray(notes.notes);

        assert.equal(result, true);
    });

    it("saveNotes should save a JSON with interface NoteFileStructure", () => {
        const notesPath: string = path.join(folders.path, "fileTests", "notes.json");
        saveNotes(
            notesPath,
            { "notes": [new Note(0, "test text", "test date", null, false, notesPath)] }
        );
        const notes: NoteFileStructure = getNotes(notesPath);
        const result: boolean = notes.notes[0].text === "test text";

        assert.equal(result, true);
    });

    it("copyNote should copy a note to an existing file in a diferent one", () => {
        const notesPath: string = path.join(folders.path, "fileTests", "notes.json");
        const notesJSON: NoteFileStructure = getNotes(notesPath);

        const moveNoteFile: File = createFile("moveTests", "moveNoteFile");
        copyNote(moveNoteFile.path, notesJSON.notes[0]);

        const movedNoteJSON: NoteFileStructure = getNotes(moveNoteFile.path);
        const result: boolean = movedNoteJSON.notes[0].text === "test text";

        assert.equal(result, true);
    });

    it("deleteNote should remove a note from a specified file", async () => {
        const notesPath: string = path.join(folders.path, "filesTests", "deleteNote.json");
        const note: Note = new Note(0, "delete note", "test date", null, false, notesPath);
        saveNotes(
            notesPath,
            { "notes": [note] }
        );

        deleteNote(note.id, notesPath);

        const deletedNote: NoteFileStructure = await getNotes(notesPath);
        const result: boolean = deletedNote.notes.length === 0;


    });

    it("replaceNote should replace an existing note with a new one", async () => {
        const notesPath: string = path.join(folders.path, "filesTests", "replaceTestNotes.json");
        const oldNote: Note = new Note(0, "original text", new Date(1995, 11, 17).toDateString(), null, false, notesPath);
        const newNote: Note = new Note(10, "replaced text", new Date().toString(), null, true, notesPath);
        saveNotes(
            notesPath,
            { "notes": [oldNote] }
        );
        replaceNote(newNote, oldNote.id);

        const replacedNotes: Note[] = getNotes(notesPath).notes;

        const result: boolean = Object.keys(replacedNotes[0]).every(key => {
            switch (key) {
                case 'index':
                    return replacedNotes[0].index === 0;
                default:
                    return replacedNotes[0][key] === newNote[key];
            }
        });

        assert.equal(result, true);
    });


    it("getAllNotes should return all existing notes", async () => {
        const allNotes: Note[] = await getAllNotes();
        const result: boolean = allNotes.length > 0;

        assert.equal(result, true);

    });

    it("getRecentNotes shuoukd return an array o notes ordere by date", async () => {
        const notesPath: string = path.join(folders.path, "filesTests", "recentTestNotes.json");
        saveNotes(
            notesPath,
            { "notes": [new Note(0, "recent text", new Date(1995, 11, 17).toDateString(), null, false, notesPath)] }
        );

        const recentNotes: Note[] = await getRecentNotes();
        const result: boolean = Date.parse(recentNotes[0].date) > Date.parse(recentNotes[recentNotes.length - 1].date);

        assert.equal(result, true)
    });

    it("getFavoriteNotes should return an array only of notes where favorites === true", async () => {
        const notesPath: string = path.join(folders.path, "fileTests", "favoriteNotes.json");
        saveNotes(
            notesPath,
            { "notes": [new Note(0, "favorite text", "test date", null, true, notesPath)] }
        );

        const favoriteNotes: Note[] = await getFavoriteNotes();
        const result: boolean = favoriteNotes.every(note => note.favorites === true);

        assert.equal(result, true);
    });

    it("getMatchNotes should return and array with the notes which text that match an specified string", async () => {
        const matchedNotes: Note[] = await getMatchNotes("favorite");
        const result: boolean = matchedNotes.every(note => note.text.match("favorite"));

        assert.equal(result, true);
    });
});

removeFolder("renamedFolder").then(() => {
    folders.getEntities().then(entities => {
        console.log("Remove renamedFolder: ", entities);
    });
});

folders.getFolder("fileTests").clear().then(() => {
    folders.getFolder("fileTests").getEntities().then(entities => {
        console.log("Clear fileTests: ", entities);
    });
});

folders.getFolder("moveTests").clear().then(() => {
    folders.getFolder("moveTests").getEntities().then(entities => {
        console.log("Clear moveTests: ", entities);
    });
});

knownFolders.documents().getEntities().then(entities => {
    console.log("Documents: ", entities);
});