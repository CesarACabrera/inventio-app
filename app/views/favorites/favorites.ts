import { EventData, fromObject } from "@nativescript/core/data/observable";
import { Page } from "@nativescript/core/ui/page";
import { ItemEventData } from "@nativescript/core";
import { editorModal, exportedDialog, fab, fsOptionsModal, setFavorite, State } from "~/modules/utilities";
import { exportAllNotes } from "~/modules/export";
import * as permissions from 'nativescript-permissions';
import { getFavoriteNotes } from "~/modules/fileSystem";

let page: Page;

export async function onLoaded(args: EventData) {
    page = <Page>args.object;
    page.bindingContext = fromObject({
        visibility: "hidden",
        notes: []
    });

    /* If getFavoriteNotes() does not return nothing, it crashes, that's why it 
    needs to be setted after the context is declared, so if it crashes, it generates the context before.*/
    page.bindingContext.set("notes", await getFavoriteNotes());

    if (page.bindingContext.notes.length > 0) {
        page.bindingContext.set("visibility", "visible");
    }

    fab(page);
}

export async function onStarTap(args: EventData) {
    setFavorite(page, args);
}

export function openEditor(args: ItemEventData) {
    editorModal(page, args);
}

export function onItemSelected(args: EventData) {
    const state: State = {
        context: [
            { text: "Exportar ", fileType: "nota" },
            { text: "Mover ", fileType: "nota" },
            { text: "Borrar ", fileType: "nota" }
        ],
        page: page,
        extras: ["favorites"]
    }
    fsOptionsModal(args, state);
}

export function exportNotes() {
    permissions.requestPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE).then(
        async function success() {
            exportedDialog("documento", await exportAllNotes(page.bindingContext.notes, "favoritas"));
        },
        function error() {
            //console.log("Permission denied.");
        }
    );
}