import { ItemEventData } from "@nativescript/core";
import { EventData, fromObject } from "@nativescript/core/data/observable";
import { Page } from "@nativescript/core/ui/page";
import { getFolders } from "~/modules/fileSystem";
import { createEntity, fab, fsOptionsModal, State } from "~/modules/utilities";

let page: Page;
const createContext = { fileType: "a carpeta", hintType: " la carpeta" };

export async function onLoaded(args: EventData) {
    page = <Page>args.object;
    page.bindingContext = fromObject({
        folders: await getFolders()
    });

    fab(page);
}

export function onFabTap(args: EventData) {
    createContext.fileType = "a carpeta";
    createEntity(page, args, createContext);
}

export function onListItemTap(args: ItemEventData) {
    page.frame.navigate({
        moduleName: "~/views/documents/documents",
        context: { folderName: page.bindingContext.folders[args.index].name },
        transition: {
            name: "fade"
        }
    });
}

export function onItemSelected(args) {
    const state: State = {
        context: [
            { text: "Exportar ", fileType: "carpeta" },
            { text: "Renombrar ", fileType: "carpeta" },
            { text: "Borrar ", fileType: "carpeta" }
        ],
        page: page,
        createContext: createContext,
    }

    fsOptionsModal(args, state);

}