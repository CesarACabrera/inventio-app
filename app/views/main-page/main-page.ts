import { ActionItem, Dialogs, EventData, fromObject, Page, ShowModalOptions } from "@nativescript/core";
import { exportAllFolders } from "~/modules/export";
import { exportedDialog } from "~/modules/utilities";
import * as permissions from 'nativescript-permissions';
import { setTheme } from "~/main-view-model";
import Theme from "@nativescript/theme";//
import { getFolders } from "~/modules/fileSystem";

let page: Page;

export function onLoaded(args: EventData) {
    page = <Page>args.object;
    page.bindingContext = fromObject({
        theme: setTheme.getTheme,
        tabColor: setTheme.getTheme === 'ns-dark' ? '#dc6f00' : '#f57c00',
        themeText: setTheme.getTheme === 'ns-dark' ? 'claro' : 'oscuro'
    })

    if (page.bindingContext.theme === "ns-dark") {
        Theme.setMode(Theme.Dark);
    }
}

export function exportContent() {
    permissions.requestPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE).then(
        async function success() {
            if ((await getFolders()).length > 0) {
                exportedDialog("contenido", await exportAllFolders());
            } else {
                const alertOptions = {
                    title: "Error al exportar",
                    message: "No hay contenido para exportar.",
                    okButtonText: 'Aceptar',
                    cancelable: true
                }

                Dialogs.alert(alertOptions);
            }

        },
        function error() {
            //console.log("Permission denied.");
        }
    );
}

export function openSearchModal(args: EventData) {
    const item: ActionItem = <ActionItem>args.object;

    const options: ShowModalOptions = {
        context: {},
        closeCallback: () => { },
        fullscreen: true
    }

    item.showModal("~/modals/searchModal/searchModalFrame", options);
}

export function setDarkMode() {
    Theme.toggleMode();
    setTheme.changeTheme();
    page.bindingContext.set("tabColor", setTheme.getTheme === 'ns-dark' ? '#dc6f00' : '#f57c00');
    setTimeout(() => {
        page.bindingContext.set("themeText", setTheme.getTheme === 'ns-dark' ? 'claro' : 'oscuro');
    }, 500)

}