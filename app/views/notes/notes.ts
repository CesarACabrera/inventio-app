import { EventData, fromObject } from "@nativescript/core/data/observable";
import { Page } from "@nativescript/core/ui/page";
import { ItemEventData } from "@nativescript/core";
import { getNotes } from "~/modules/fileSystem";
import { editorModal, fab, fsOptionsModal, setFavorite, State } from "~/modules/utilities";

let page: Page;
let document: {
    path: string,
    name: string,
    extension: string
}

export async function onLoaded(args: EventData) {
    page = <Page>args.object;
    document = page.navigationContext.document;
    page.bindingContext = fromObject({
        notes: await getNotes(document.path).notes,
    });

    fab(page);
}

export async function onStarTap(args: EventData) {
    setFavorite(page, args);
}

export function openEditor(args: ItemEventData) {
    editorModal(page, args, document.path);
}

export function onItemSelected(args: EventData) {
    const state: State = {
        context: [
            { text: "Exportar ", fileType: "nota" },
            { text: "Mover ", fileType: "nota" },
            { text: "Borrar ", fileType: "nota" }
        ],
        page: page,
        extras: ["notes"]
    }
    fsOptionsModal(args, state);
}