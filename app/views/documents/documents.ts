import { ItemEventData } from "@nativescript/core";
import { EventData, fromObject } from "@nativescript/core/data/observable";
import { Page } from "@nativescript/core/ui/page";
import { getFiles } from "~/modules/fileSystem";
import { createEntity, fab, fsOptionsModal, State } from "~/modules/utilities";

let page: Page;
let folderName: string;
let createContext = { fileType: "o documento", hintType: "l documento" };

export async function onLoaded(args: EventData) {
    page = <Page>args.object;
    folderName = page.navigationContext.folderName;
    page.bindingContext = fromObject({
        documents: await getFiles(folderName)
    });

    fab(page);
}

export function onFabTap(args: EventData) {
    createContext.fileType = "o documento";
    createEntity(page, args, createContext, folderName);
}

export function onListItemTap(args: ItemEventData) {
    page.frame.navigate({
        moduleName: "~/views/notes/notes",
        context: { document: page.bindingContext.documents[args.index] },
        transition: {
            name: "fade"
        }
    });
}

export function onItemSelected(args: EventData) {
    const state: State = {
        context: [
            { text: "Exportar ", fileType: "documento" },
            { text: "Renombrar ", fileType: "documento" },
            { text: "Mover ", fileType: "documento" },
            { text: "Borrar ", fileType: "documento" }
        ],
        page: page,
        createContext: createContext,
        extras: [folderName]
    }

    fsOptionsModal(args, state);
}