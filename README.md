# Inventio

Inventio es una aplicación de software libre que permite escribir notas, editarlas y agregarles imágenes. 
Es posible organizar el contenido en documentos y estos, a su vez, en carpetas. Además, éstas se pueden consultar por categorías, recientes y favoritas. También incluye un buscador para encontrar notas más rápidamente. Finalmente, permite exportar el contenido, ya sean notas, documentos o carpetas en formato .docx. Incluye un tema oscuro.

Inventio is a free-software application that allows you to write notes, edit them and add images to them. It is possible to organize the content in documents and these, in turn, in folders. In addition, these can be consulted by categories, recent and favorites. It also includes a search engine to find notes more quickly. Finally, it allows you to export the content, be it notes, documents or folders in .docx format. Includes a dark theme. 

![](./assets/screenshots.png)