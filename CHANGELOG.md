# Change log

All notable changes to this project will be documented in this file.

## [1.0.1] - 2021-08-29
### Fixed
- Fixed bugs while deleting notes notes.

## [1.0.0] - 2021-08-11
### Added
- Added all basic features of the Inventio app.

[1.0.1]: https://gitlab.com/CesarACabrera/inventio-app/-/tags/1.0.1
[1.0.0]: https://gitlab.com/CesarACabrera/inventio-app/-/tags/1.0.0